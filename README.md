
Media Query Images
==================

A javascript-free responsive image formatter.

This module has similar functionality to the [Picture] module.

[Picture]: https://drupal.org/project/picture

This module provides an additional image field formatter and stand-alone widget
that allows the browser to automatically download the appropriately sized image
for the current display. This helps save bandwidth and improves the page load
time.

Because the module does not require javascript in modern browsers, images are
requested sooner in page load and reduces browser processor usage on javascript.

Instructions
------------

After enabling the module you can select it as a field formatter for any image
fields defined on a content type.

There are three options when using this module as a field formatter:

1.  "Media Query Images with Picture mappings" - This allows formatting of the
    field using pre-existing mappings from the Picture module. This allows for
    simple replacement of Picture module formatting with this module. Picture
    module must remain enabled to preserve breakpoint mappings.
    
    Requires [Picture] and [Breakpoints] module.

2.  "Media Query Images with Breakpoints" - This allows formatting of the field
    with Breakpoints functionality. Each breakpoint in Breakpoints group is
    configured in the field display settings.
    
    Requires [Breakpoints] module.

3.  "Media Query Images" - This output has no automatic image choosing
    functionality. You select from the list of available styles for this field
    to show. This output is designed for developers that want to have total
    control of the displayed image style using their code.
    
    Requires no additional modules.


[Breakpoints]: https://drupal.org/project/breakpoints


Legacy Browser Support
----------------------

The built-in automatic image selection funcitonality of this module depends on
media queries support in the browser.

Browsers supporting media queries can be seen here
http://caniuse.com/#feat=css-mediaqueries.

This functionality also works with IE8 when using [respond.js], a JS polyfill for
media queries.

Tested using [Respond.js module] in IE8. Does not require CSS aggregation to be
enabled.

Not tested in any older browsers.

[respond.js]: https://github.com/scottjehl/Respond
[Respond.js module]: https://drupal.org/project/respondjs
